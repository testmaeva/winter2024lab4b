public class Dragon{
	private boolean wings;
	private boolean horns;
	private boolean spitsFire;
	
	/*public String blowsFire;
	public String hashorns;
	public String canFly;*/
	
	public Dragon(boolean wings, boolean horns, boolean spitsFire){
		this.wings = wings;
		this.horns = horns;
		this.spitsFire = spitsFire;
	}
	
	public boolean getWings (){
		return this.wings;
	}
	public boolean getHorns (){
		return this.horns;
	}
	public void setHorns (boolean horns){
		this.horns = horns;
	}
	public boolean getSpitsFire (){
		return this.spitsFire;
	}
	
	
	/*public String dragonFire(){
		if(spitsFire == true){
			blowsFire = "🌬 I know that's right";
		} else{
			blowsFire = "Side eye...";
		}
		return blowsFire;
	}
	public String hasWings(){
		if(wings == true){
			canFly = "Alexa, play I'm levitating by Dua Lipa";
		} else{
			canFly = "Just because you can't fly doesn't mean I also can't";
		}
		return canFly;
	}
	public String dragonHornSupremacy(){
		if(horns == true){
			hashorns = "In horns we believe 😈😈";
		} else{
			hashorns = "Do I look like I wouldn't have horns?";
		}
		return hashorns;
	}*/
}