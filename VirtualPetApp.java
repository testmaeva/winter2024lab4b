import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Dragon[] mythicalCreatures = new Dragon[4];
		System.out.println("Please input true or false for each statement");
	
		for(int i = 0; i < 1; i++){
			
				
			System.out.println("Can it spit fire");
			boolean breathesFire = reader.nextBoolean();
			
			System.out.println("Can it fly");
			boolean canFly = reader.nextBoolean();
		
			System.out.println("Does it not have horns");
			boolean hasHorns = reader.nextBoolean();

			mythicalCreatures[i] = new Dragon(canFly, hasHorns, breathesFire);
		}
		System.out.println("Lemme ask again, does it NOT have horns");
		mythicalCreatures[0].setHorns(reader.nextBoolean());
		
		System.out.println("Wings: " +mythicalCreatures[0].getWings());
		System.out.println("Horns: " +mythicalCreatures[0].getHorns());
		System.out.println("Spits fire: " +mythicalCreatures[0].getSpitsFire());
	}
}